import {Navbar, Nav, Container} from 'react-bootstrap'
import {Fragment, useContext} from 'react'
import {Link} from 'react-router-dom'
import UserContext from '../UserContext'


export default function AppNavBar(){

  const {user} = useContext(UserContext)
  console.log(user)

	return(
		<Navbar collapseOnSelect expand="lg" bg="dark" variant="dark" className="sticky-top" >
  <Container className="navbar navbar-expand-lg navbar-dark  rounded-pill">
  <Navbar.Brand as={Link} to="/">WindowShoping</Navbar.Brand>
  <Navbar.Toggle aria-controls="responsive-navbar-nav" />

  <Navbar.Collapse id="basic-navbar-nav">
   <Nav className="me-auto">

     <Nav.Link as={Link} to="/">Home</Nav.Link>
     <Nav.Link as={Link} to="/products">Products</Nav.Link>
     
     {
       user.id !== null ?
       <Nav>
         
         <Nav.Link as={Link} to="/logout">Logout</Nav.Link>
         </Nav>
       :
       <Nav>
         <Nav.Link as={Link} to="/register">Register</Nav.Link>
     <Nav.Link as={Link} to="/login">Login</Nav.Link>
       </Nav>

     }
    
    
   </Nav>
 </Navbar.Collapse>
  </Container>
</Navbar>
	);
};